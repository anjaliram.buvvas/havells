<?php require_once("include/connectingdb.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php require_once("include/session.php"); ?>

<!--------------------------Delete query start from here--------------------------->
  <?php
    
   if (isset($_POST['submit'])){
     $searchqueryparameter = $_GET['id'];
     global $connectingdb;
     $sql = "SELECT * FROM products WHERE id = '$searchqueryparameter'";
     $stmt = $connectingdb->query($sql);
     while($datarows=$stmt->fetch()){
       $image = $datarows['image'];
     } 
    // query to Delete post in database when everything is fine
    $sql = "DELETE FROM products WHERE id = '$searchqueryparameter'";
    $execute = $connectingdb->query($sql);  
    if($execute){
      $target_path_to_delete_image = "uploads/$image";// when delete product in databse image will delete in upload file also
      unlink($target_path_to_delete_image);
      $_SESSION['successmessage']="Product Deleted successfully";
      redirect_to("allproduct.admin.php");
    }else{
      $_SESSION['errormessage']="something went wrong. try again !";
      redirect_to("allproduct.admin.php");
    }
  }
?> 
<!--------------------------Delete query end here--------------------------->
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>index</title>
     <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>
<body>
   <?php include_once("header.php");?>
  <!-------------------------------------LEFT SIDE NAVBAR START---------------- ----------------->
  <div class="">
    <div class="row">
      <div class="col-md-3">
       <main>
          <div class="d-flex flex-column flex-shrink-0 p-3 text-white" style="height:2500px">
            <ul class="nav">
              <li class="nav-item">
                <a href="newproduct.admin.php" class="text-decoration-none px-4 py-3 d-block">
                 <i class="fas fa-folder-plus"></i> Add New Product
               </a><hr>
             </li>
             <li class="nav-item">
                <a href="allproduct.admin.php" class="text-decoration-none px-4 py-3 d-block">
                  <i class="fas fa-edit"></i> View all Products
                </a><hr>  
              </li>
              <li class="nav-item">
                <a href="havells_project" class="text-decoration-none px-4 py-3 d-block" target="_blank">
                <i class="fas fa-blog"></i> View Blog
                </a><hr>
              </li>
            </ul>
         </div>
       </main>
     </div>
     <!-------------------------------------LEFT SIDE NAVBAR END---------------- -----------------> 
     <!-------------------------------------RIGHT MAIN AREA START---------------- ----------------->
      <div class="col-lg-9 mt-4">

         <!-- -------------------all products main area----------------------------- -->
         <div class="container">
              <div class="row">
                  <div class="col-lg-11">
                    <?php
                       echo errormessage();
                       echo successmessage();
                      ?>
                      <table class="table table-striped table-hover">
                         <thead class="thead-dark">
                              <tr>
                                  <th scope="col">#</th>
                                  <th scope="col">Image</th>
                                  <th scope="col">Modal Name</th>
                                  <th scope="col">Price</th>
                                  <th scope="col">Update</th>
                                  <th scope="col">Delete</th>
                              </tr>
                          </thead>
                          <?php
                             global $connectingdb;
                             $sql = "SELECT * FROM products";
                             $stmt = $connectingdb->query($sql);
                             $sr = 0;
                             while($datarows=$stmt->fetch()){
                                $id = $datarows['id'];
                                $image = $datarows['image'];
                                $price = $datarows['price'];
                                $brand = $datarows['brand'];
                               $sr++;
                               $myname="exampleModal".$id;
                           ?>
                            <tbody>
                                <tr >
                                 <td class="pt-5"><?php echo $sr;?></td>
                                 <td><img src="uploads/<?php echo $image;?>" alt="" width="150px" height="100px"></td>
                                 <td class="pt-5"><?php echo $brand;?></td>
                                 <td class="pt-5"><?php echo $price;?> &#8377; </td>
                                 <td class="pt-5"><a href="edit.admin.php?id=<?php echo $id ;?>" target=""><span class="btn btn-primary text-white" style="padding:6px 25px">Edit</span></a></td>
                                 <td class="pt-5"><button type="button" name="delete" class="btn btn-danger" data-target="#<?= $myname;?>" data-toggle="modal" value="Delete">delete</buttom></td>
                                  <!--------modal display start here-------->
                                  <div class="modal"  id="<?= $myname;?>">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <!------modal header start--------->
                                        <div class="modal-header">
                                          <h5 class="modal-title">Modal title</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                           <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <!------modal body start--------->
                                        <div class="modal-body">
                                          <p>are you sure you want to delete this <?= $brand;?></p>
                                        </div>
                                        <!------modal footer start--------->
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                          <form action="allproduct.admin.php?id=<?= $id?>" method="POST">
                                             <a href="allproduct.admin.php?id=<?= $id?>"><input type="submit" class="btn btn-primary"  name="submit" value="Ok"></a>
                                          </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div> 
                                </tr>
                            </tbody> 
                          <?php } ?>  
                      </table>
                    </div>
               </div>
            </div>
           <!-- ------------------- all products main area----------------------------- -->      
        </div>
      <!-------------------------------------RIGHT MAIN AREA END---------------- ----------------->
    </div>
  </div>
 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <!-- <script>
    function checkdelete(){
      return confirm('Are you sure you want to delete the Product?');
    }
 </script>  -->
</body>
</html>
