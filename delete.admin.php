<?php require_once("include/connectingdb.php");?>
<?php require_once("include/functions.php");?>
<?php require_once("include/session.php");?>
<!-- <?php  echo '<script language="javascript">';
  echo 'alert("message successfully sent")';
  echo '</script>';?> -->
<?php
 $searchqueryparameter = $_GET['id'];
 if(isset($_POST['delete'])){
 
    // query to Delete post in database when everything is fine
    global $connectingdb;
    $sql = "DELETE FROM products WHERE id = '$searchqueryparameter'";
    $execute = $connectingdb->query($sql); 
     // Display the alert box  
    if($execute){
      $_SESSION['successmessage']="post Deleted successfully";
      redirect_to("allproduct.admin.php");
    }else{
      $_SESSION['errormessage']="something went wrong. try again !";
      redirect_to("allproduct.admin.php");
    }
}
?> 
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>index</title>
     <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>
<body>
  <?php include_once("header.php");?>
  <!-------------------------------------LEFT SIDE NAVBAR START---------------- ----------------->
  <div class="">
    <div class="row">
      <div class="col-md-3">
       <main>
          <div class="d-flex flex-column flex-shrink-0 p-3 text-white bg-primary" style="width: 280px;height:800px">
            <ul class="nav">
              <li>
                <a href="newproduct.admin.php" class="btn btn-primary btn-block">
                 <i class="fas fa-folder-plus"></i> Add New Product
               </a><hr>
             </li>
             <li class="nav-item">
                <a href="allproduct.admin.php" class="btn btn-primary btn-block">
                  <i class="fas fa-edit"></i> View all Products
                </a><hr>  
              </li>
              <li>
                <a href="viewblog.php" class="btn btn-primary btn-block" target="_blank">
                <i class="fas fa-blog"></i> View Blog
                </a><hr>
              </li>
            </ul>
         </div>
       </main>
     </div>
     <!-------------------------------------LEFT SIDE NAVBAR END---------------- -----------------> 
     <!-------------------------------------RIGHT MAIN AREA START---------------- ----------------->
      <div class="col-lg-9 mt-5">
        <div class="container">
           <div class="row">
              <div class="col-md-8 ml-5" style="box-shadow:0 0 25px #ddd;padding:50px;">
                 <?php
                   echo errormessage();
                   echo successmessage();
                    //Fetching Existing Content according to our
                    global $connectingdb;
                    $sql = "SELECT * FROM products WHERE id = '$searchqueryparameter'";
                    $stmt = $connectingdb->query($sql);
                    while($datarows=$stmt->fetch()){
                    $imagedb= $datarows['image'];
                    $pricedb = $datarows['price'];
                   }
                 ?>
                  <form action="delete.admin.php?id=<?php echo $searchqueryparameter;?>" method="POST" enctype= "multipart/form-data">
                      <div class="form-group">
                         <input type="file"  accept="image/*" name="image" id="file"  onchange="loadFile(event)" style="display: none;">
                         <label for="file" style="cursor: pointer;" class="btn btn-danger">Upload Image </label> 
                         <div style="border:1px solid #ddd;height:180px;width:300px">
                         <img id="output" width="300" height="180px" src="uploads/<?php echo $imagedb;?>"/>
                        </div>
                         <script>
                             var loadFile = function(event) {
	                         var image = document.getElementById('output');
	                         image.src = URL.createObjectURL(event.target.files[0]);
                             };
                         </script>
                      </div><br>
                      <div class="form-group">
                         <input  class="form-control" style="width:460px" type="text" name="price" id="title" placeholder="enter price" value="<?php echo $pricedb?>">
                      </div><br>
                       <button type="submit" name="delete" class="btn btn-danger border-0"><i class="fas fa-trash"></i> Delete</button>
                   </form>
              </div>
          </div>
       </div>
      </div>
     <!-------------------------------------RIGHT MAIN AREA END---------------- ----------------->
    </div>
   </div>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
