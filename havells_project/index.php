<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- ---------------css files------------ -->
    <link rel="stylesheet" href = "css/navbar.css">
    <link rel="stylesheet" href = "css/footer.css">
    <link rel="stylesheet" href = "css/contactus.css">
    <link rel="stylesheet" href = "css/home.css">
    <title>Havells</title>
</head>
<body>
  <!------------------------------------NAVBAR------------------------------------->
 <?php include('header.php');?>
 <!----------------------------PHP CODE FOR MOVE TO ANOTHER FILE---------------------- --->
 <div id = "content">
        <?php
        // $PageDirectory = 'myphpfiles';
        //  if(!empty($_GET['pagename'])){
        //    $pagename = $_GET['pagename'];
        //    $PagesFolder = scandir($PageDirectory);
        //    if(in_array($pagename.".php",$PagesFolder)){
        //        include($PageDirectory."/".$pagename.".php");
        //    }else{
        //     echo "<h1>404 Error Page!</h1>";
        //     echo "<h2>Page Not Found</h2>";
        //     echo "<h3>Search Again With Valid URL</h3>";    
        //    }
        //  }else{
        //      include($PageDirectory."/home.php");
        //  }
        include('myphpfiles/home.php');
        include('myphpfiles/about.php');
        include('myphpfiles/contactus.php');
        ?>
  </div><br>
 <!-----------------------------------------FOOTER--------------------------------- -->
 <?php
  include('footer.php');
 ?>
 <!-------------watsapp intigration start here-------------------->
<script>
    var url = 'https://wati-integration-service.clare.ai/ShopifyWidget/shopifyWidget.js?97193';
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = url;
    var options = {
  "enabled":true,
  "chatButtonSetting":{
      "backgroundColor":"#4dc247",
      "ctaText":"",
      "borderRadius":"25",
      "marginLeft":"0",
      "marginBottom":"50",
      "marginRight":"50",
      "position":"right"
  },
  "brandSetting":{
      "brandName":"WATI",
      "brandSubTitle":"Typically replies within a day",
      "brandImg":"https://cdn.clare.ai/wati/images/WATI_logo_square_2.png",
      "welcomeText":"Hi there!\nHow can I help you?",
      "messageText":"Hello, I have a question about {{page_link}}",
      "backgroundColor":"#0a5f54",
      "ctaText":"Start Chat",
      "borderRadius":"25",
      "autoShow":false,
      "phoneNumber":"" //we shoud give a phone number here with country code ex:- "912685959115"
  }
};
    s.onload = function() {
        CreateWhatsappChatWidget(options);
    };
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
</script>
<!-------------watsapp intigration end here-------------------->
 </body>
</html>