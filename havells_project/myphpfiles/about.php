<div calss="homepage" id="about">
<div class="hroimg">
         <img src="images\Fans_Inner_Banner.gif" alt="" height="400px" width="100%">
</div>
 <div class="container">
     <div class="row">
         <h2 class="fans-heading">FANS</h2>
          <p class="home-content">A producer of numerous types of fans including exquisite, antique finishes to fans specially designed for kids to dual color fans and High Speed Fans, Havells entered the fan business in mid-2003 and since then, has never looked back. With innovative designs and excellent finishes being the key elements, Havells has successfully been able to capture the essence of the customers' needs. An important appliance for every household, it is important that a fan functions properly giving sufficient air flow. Be it a wall fan, ceiling fan, table fan, an exhaust or a personal fan, the entire range from Havells is built with the best materials to give optimum results and last long. Apart from the Havells guarantee, there are multiple other benefits that you can avail. The biggest advantage is that you can get fans online and not worry about going to different stores and spending time. Check out the latest fan range online and select what suits you the best. We bring forward a collection of classic, stylish and efficient fans that you can install in your homes to make them beautiful. Match the fans with your furniture, walls, curtains etc. and find relevant options for your living room, bedroom, dining, balconies, offices, shops and more.</p> 
     </div>
 </div><br>
     <!----------------------------IMAGE GALLERY---------------------- -->
    <div class="container-fluid"> 
     <div class=row>
        <div class = col-md-3>
            <div class="card" style="width: 18rem;">
             <img src="images/fan1.png" class="card-img-top" alt="" height="300px">
              <div class="card-body">
                  <a href="./products.php" class="btn btn-primary">View Product</a>
              </div>
          </div>
       </div>
       <div class = col-md-3>
            <div class="card" style="width: 18rem;">
             <img src="images/fan2.png" class="card-img-top" alt="..." height="300px">
              <div class="card-body">
                  <a href="./products.php" class="btn btn-primary">View Product</a>
              </div>
          </div>
       </div>
       <div class = col-md-3>
            <div class="card" style="width: 18rem;">
             <img src="images/fan3.png" class="card-img-top" alt="..." height="300px">
              <div class="card-body">
                  <a href="./products.php" class="btn btn-primary">View Product</a>
              </div>
          </div>
       </div>
       <div class = col-md-3>
            <div class="card" style="width: 18rem;">
             <img src="images\fan4.png" class="card-img-top" alt="..." height="300px">
              <div class="card-body">
                  <a href="./products.php" class="btn btn-primary">View Product</a>
              </div>
          </div>
       </div>
    </div>
 </div>

 <div class="container-fluid"> 
     <div class=row>
        <div class = col-md-3>
            <div class="card" style="width: 18rem;">
             <img src="images/fana.png" class="card-img-top" alt="" height="300px">
              <div class="card-body">
                  <a href="./products.php" class="btn btn-primary">View Product</a>
              </div>
          </div>
       </div>
       <div class = col-md-3>
            <div class="card" style="width: 18rem;">
             <img src="images/fanb.png" class="card-img-top" alt="..." height="300px">
              <div class="card-body">
                  <a href="./products.php" class="btn btn-primary">View Product</a>
              </div>
          </div>
       </div>
       <div class = col-md-3>
            <div class="card" style="width: 18rem;">
             <img src="images/fanc.png" class="card-img-top" alt="..." height="300px">
              <div class="card-body">
                  <a href="./products.php" class="btn btn-primary">View Product</a>
              </div>
          </div>
       </div>
       <div class = col-md-3>
            <div class="card" style="width: 18rem;">
             <img src="images\fand.png" class="card-img-top" alt="..." height="300px">
              <div class="card-body">
                  <a href="./products.php" class="btn btn-primary">View Product</a>
              </div>
          </div>
       </div>
    </div>
 </div>
</div>