<?php require_once("../include/connectingdb.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- ---------------css files------------ -->
    <link rel="stylesheet" href = "css/navbar.css">
    <link rel="stylesheet" href = "css/footer.css">
    <link rel="stylesheet" href = "css/contactus.css">
    <link rel="stylesheet" href = "css/home.css">
    <title>Havells</title>
</head>
<body>
   <!----------------------------------TOPNAVBAR---------------------------------- -->
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" >
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
      <img src="images/logo.png" alt="" width="100%" height="auto">
    </a>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <ul class="navbar-nav mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="index.php?#home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="index.php?pagename=#about">about</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="index.php?pagename=#contactus">contactus</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="products.php">products</a>
        </li>
     </ul>
    </div>
  </div>
 </nav><br><br><br>
 <!-----------------------HERO IMAGE------------------------>
 <!-------------------------------------MAIN AREA START---------------- ----------------->
 <div class="container-fluid">
      <div class="row">
          <div class="offset-lg-1 col-lg-10 text-center">
             <?php
              global $connectingdb;
              $sql = "SELECT * FROM products";
              $stmt = $connectingdb->query($sql);
              while($datarows=$stmt->fetch()){
                  $image = $datarows['image'];
                  $price = $datarows['price'];
                  $brand = $datarows['brand'];
             ?>
                  <div class="card mt-5" style="width: 18rem;border:none;float:left;">
                     <img src="../uploads/<?php echo $image;?>" class="card-img-top" alt="" height="300px">
                     <p style="padding-top:20px;font-size:1.1rem"><?php echo $brand; ?></p>
                     <p style="font-size:20px">&#8377;<?php echo $price; ?></p>
                     <div class="card-body">
                        <a href="#" class="btn btn-danger ml-5">Add Product</a>
                     </div>
                  </div>
              
              <?php } ?>
            </div>
        </div>
    </div><br>
  <!-------------------------------------MAIN AREA END---------------- ----------------->
  <!-----------------------------------------FOOTER--------------------------------- -->
 <?php
  include('footer.php');
 ?>
 <!-------------watsapp intigration start here-------------------->
<script>
    var url = 'https://wati-integration-service.clare.ai/ShopifyWidget/shopifyWidget.js?97193';
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = url;
    var options = {
  "enabled":true,
  "chatButtonSetting":{
      "backgroundColor":"#4dc247",
      "ctaText":"",
      "borderRadius":"25",
      "marginLeft":"0",
      "marginBottom":"50",
      "marginRight":"50",
      "position":"right"
  },
  "brandSetting":{
      "brandName":"WATI",
      "brandSubTitle":"Typically replies within a day",
      "brandImg":"https://cdn.clare.ai/wati/images/WATI_logo_square_2.png",
      "welcomeText":"Hi there!\nHow can I help you?",
      "messageText":"Hello, I have a question about {{page_link}}",
      "backgroundColor":"#0a5f54",
      "ctaText":"Start Chat",
      "borderRadius":"25",
      "autoShow":false,
      "phoneNumber":"" //we shoud give a phone number here with country code ex:- "912685959115"
  }
};
    s.onload = function() {
        CreateWhatsappChatWidget(options);
    };
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
</script>
<!-------------watsapp intigration end here-------------------->
</body>
</html>