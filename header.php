 <!------------------------------TOP NAVBAR START------------------------------------------>
 <nav class="bg-light px-5 py-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3">
          <a href="#"  class=" w-100"><img src="images/logo.png" alt=""></a>
        </div>
        <div class="offset-lg-7 col-lg-2 d-block pt-2">
          <div class="dropdown">
            <button class="btn dropdown-toggle ml-5 btn-outline-danger" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-expanded="false"  style="font-weight:bold;text-decoration:none;">
            <i class="fas fa-user"></i> Admin
            </button>
            <ul class=" btn dropdown-menu bg-white" aria-labelledby="dropdownMenuButton1">
              <li><a class="btn dropdown-item text-danger btn-outline-light" href="profile.php">My Profile</a></li>
              <li><a class="btn dropdown-item text-danger btn-outline-light" href="logout.php">logout</a></li>
            </ul>
          </div>
        <div>
      </div>
    </div>
  </nav>
  <!-------------------------------------TOP NAVBAR END---------------- ----------------->