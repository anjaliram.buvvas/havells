<?php
 session_start();
 function errormessage(){
     if(isset($_SESSION['errormessage']))
     {
        $output = "<div class=\"alert alert-danger\">"; //here we use escape charecter "\" becauese we use double qutes inside double quotes
        $output .= htmlentities($_SESSION['errormessage']);//this function protect html syntacx without braking while using html in output
        $output .= "</div>";
        $_SESSION['errormessage'] = null; //this statement makes not showing error message all the time mean clear error message when realode
        return $output;
    }
 }

 function successmessage(){
    if(isset($_SESSION['successmessage']))
    {
       $output = "<div class=\"alert alert-success\">"; 
       $output .= htmlentities($_SESSION['successmessage']);
       $output .= "</div>";
       $_SESSION['successmessage'] = null;
       return $output;
   }
}

?>