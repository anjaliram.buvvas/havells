<?php require_once("include/connectingdb.php")?>
<?php require_once("include/session.php")?>
<?php
 if(isset($_POST['submit'])){
   $email = $_POST['email'];
   $password = $_POST['password'];
   if(empty($email)&&empty($password)){
      $_SESSION['errormessage']="Enter email and password";
   }else{
    global $connectingdb;
    $sql = "SELECT * FROM admin";
    $stmt = $connectingdb->query($sql);
    while($datarows=$stmt->fetch()){
      $emaildb = $datarows['email'];
      $passworddb = $datarows['password'];
    }
     if(($email===$emaildb)&&($password===$passworddb)){
      header('Location:newproduct.admin.php');
     }else{
      $_SESSION['errormessage']="enter valid details";
     }
  }
 }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/loginstyle.css">
    <title>Login</title>
</head>
<body>
  <!------------------------LOGIN FORM------------------------------->
  
  <div class="container">
    <div class="row justify-content-center mt-5">
      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 mt-5 p-5">
      <?php echo errormessage();?>
        <div class="logo mb-4 text-center w-100"><img src="images/logo.png" alt=""></div>
        <form action="login.php" method="POST">
          <div class="mb-4">
            <label for="exampleInputEmail1" class="form-label">Email address</label>
            <input type="text" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" style="box-shadow:none;">
          </div>
          <div class="mb-4">
            <label for="exampleInputPassword1" class="form-label">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" style="box-shadow:none;">
         </div>
         <button type="submit" name="submit" class="btn btn-danger border-0 ">Submit</button>
         <a href="forgotpassword.php" class="forgotpswd">forgot password?</a>
        </form>
      </div>
    </div>
  </div>
  <!--------------------------------------------BOOTSTRAP CDN LINKS----------------------------------------------------->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>   
</body>
</html>