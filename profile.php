<?php require_once("include/connectingdb.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php require_once("include/session.php"); ?>
<?php
 if(isset($_POST["submit"])){
     $email = $_POST['email'];
     $password = $_POST['password'];
     if(!empty($email) && !empty($password)){
         global $connectingdb;
         $sql = "UPDATE admin SET email='$email', password='$password'";
         $execute = $connectingdb->query($sql);
         if($execute){
              $_SESSION['successmessage'] = "details updated successfully";
              redirect_to("profile.php");
           }
        }else{
         $_SESSION['errormessage'] = "email and password field cant be empty!";
       }
   }
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/loginstyle.css">
    <link rel="stylesheet" href="css/style.css">
    <title>profile</title>
     <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>
<body>
   <?php include_once("header.php");?>
  <!-------------------------------------LEFT SIDE NAVBAR START---------------- ----------------->
  <div class="">
    <div class="row">
      <div class="col-md-3">
       <main>
          <div class="d-flex flex-column flex-shrink-0 p-3 text-white" style="height:1000px">
            <ul class="nav">
              <li class="nav-item">
                <a href="newproduct.admin.php" class="text-decoration-none px-4 py-3 d-block">
                 <i class="fas fa-folder-plus"></i> Add New Product
               </a><hr>
             </li>
             <li class="nav-item">
                <a href="allproduct.admin.php" class="text-decoration-none px-4 py-3 d-block">
                  <i class="fas fa-edit"></i> View all Products
                </a><hr>  
              </li>
              <li class="nav-item">
                <a href="havells_project" class="text-decoration-none px-4 py-3 d-block" target="_blank">
                <i class="fas fa-blog"></i> View Blog
                </a><hr>
              </li>
            </ul>
         </div>
       </main>
     </div>
     <!-------------------------------------LEFT SIDE NAVBAR END---------------- -----------------> 
     <!-------------------------------------RIGHT MAIN AREA START---------------- ----------------->
  <div class="col-lg-9">
   <!------------------------LOGIN FORM------------------------------->
     <?php          //getting admin details from database
      global $connectingdb;
      $sql = "SELECT * FROM admin";
      $stmt = $connectingdb->query($sql);
      while($datarows=$stmt->fetch()){
        $usernamedb = $datarows['username'];
        $emaildb = $datarows['email'];
        $passworddb = $datarows['password'];
      }
     ?>
    <div class="row">
      <div class="offset-lg-2 col-md-6 col-lg-6 col-sm-6 p-5 mt-5">
      <?php 
      echo errormessage();
      echo successmessage();
      ?>
        <div class="logo mb-2 text-center w-100"><img src="images/logo.png" alt=""></div>
        <h6 class="text-center text-secondary  mb-4"> you can edit your email and password here!</h6>
        <form action="profile.php" method="POST">
           <h3 class="text-danger"><i class="fas fa-user"></i> <?php echo $usernamedb;?></h3><br>
          <div class="mb-4">
            <label for="exampleInputEmail1" class="form-label">Email address</label>
            <input type="text" name="email" class="form-control" id="exampleInputEmail1" style="box-shadow:none;" value="<?php echo $emaildb;?>">
          </div>
          <div class="mb-4">
            <label for="exampleInputPassword1" class="form-label">Password</label>
            <input type="text" name="password" class="form-control" id="exampleInputPassword1" style="box-shadow:none;" value="<?php echo $passworddb;?>">
         </div>
         <button type="submit" name="submit" class="btn btn-danger border-0">Save Changes</button> 
        </form>
      </div>
    </div>
 </div>
      <!-------------------------------------RIGHT MAIN AREA END---------------- ----------------->
    </div>
  </div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>
